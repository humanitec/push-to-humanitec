from bitbucket_pipes_toolkit import Pipe, get_logger
import docker
import json
import re
import requests

logger = get_logger()

schema = {
  'HUMANITEC_TOKEN': {'type': 'string', 'required': True},
  'ORGANIZATION': {'type': 'string', 'required': True},
  'MODULE_NAME': {'type': 'string', 'required': True},
  'IMAGE_NAME': {'type': 'string', 'required': True},
  'HUMANITEC_API': {'type': 'string', 'required': False, 'default': 'api.humanitec.io'},
  'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}


class PushToHumanitec(Pipe):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.docker_client = None

    def get_client(self):
      if self.docker_client is None:
        self.docker_client = docker.APIClient(base_url='unix://var/run/docker.sock')
      
      return self.docker_client
    
    def validate_variables(self, module_name):
        match = re.search('^[a-z0-9][a-z0-9-]*[a-z0-9]$', module_name)
        
        if not match:
          print("Module name must be all lowercase letters, numbers and the - symbol. It cannot start or end with -.")
          self.fail(f"Module name not valid")

    def get_registry_credentials(self, humanitec_token, humanitec_api, organization):
        headers = {'Authorization': 'Bearer ' + humanitec_token}
        response = requests.get("https://" + humanitec_api + "/orgs/" + organization + "/registries/humanitec/creds", headers=headers)
        if response.status_code != 200:
          self.fail(f"Unable to access Humanitec")

        return response.json()

    def log_in_registry(self, docker_client, registry, username, password):
        try:
          docker_client.login(registry=registry, username=username, password=password)
        except:
          print("Unable to fetch repository credentials.")
          print("Did you add the token to your Github Secrets?")
          print("http:/docs.humanitec.com/connecting-your-ci#bitbucket-pipeline")
          self.fail(f"Unable to connect to the Humanitec registry.")

    def docker_tag(self, docker_client, image_name, repository_name):
      try:
        docker_client.tag(image_name, repository_name, "BITBUCKET_COMMIT")
      except:
        print("Could not tag the Docker image.")
        self.fail(f"Unable to tag Docker image.")

    def docker_push(self, docker_client, repository_name):
      try:
        docker_client.push(repository_name, "BITBUCKET_COMMIT")
      except:
        print("Push to Humanitec registry failed.")
        self.fail(f"Unable to push to Humanitec registry.")


    def inform_humanitec(self, humanitec_token, repository_name, humanitec_api, organization, module_name):
      headers = {
        'Authorization': 'Bearer ' + humanitec_token, 
        'Content-Type': 'application/json'
      }
      body = {
        'commit': 'BITBUCKET_COMMIT',
        'image': f'{repository_name}'
      }
      response = requests.post("https://" + humanitec_api + "/orgs/" + organization + "/modules/" + module_name + "/builds", headers=headers, json=body)
      if response.status_code != 201:
        self.fail(f"Unable to add new build to Humanitec.")

    def run(self):
        super().run()

        logger.info('Executing the push-to-humanitec pipe...')

        humanitec_token = self.get_variable('HUMANITEC_TOKEN')
        organization = self.get_variable('ORGANIZATION')
        module_name = self.get_variable('MODULE_NAME')
        image_name = self.get_variable('IMAGE_NAME')
        humanitec_api = self.get_variable('HUMANITEC_API')

        # Validate variables
        self.validate_variables(module_name)

        # Get registry credentials
        registry_creds = self.get_registry_credentials(humanitec_token, humanitec_api, organization)
        username = registry_creds['username']
        password = registry_creds['password']
        registry = registry_creds['registry']

        # Login in the registry
        docker_client = self.get_client()
        self.log_in_registry(docker_client, registry, username, password)

        # Tag image
        repository_name = registry + "/"+ organization + "/" + module_name
        self.docker_tag(docker_client, image_name, repository_name)

        # Push image
        self.docker_push(docker_client, repository_name)

        # Inform Humanitec
        self.inform_humanitec(humanitec_token, repository_name, humanitec_api, organization, module_name)

        self.success(f"Imaged successfully pushed to Humanitec.")

if __name__ == '__main__':
    pipe = PushToHumanitec(pipe_metadata='/pipe.yml', schema=schema)
    pipe.run()
