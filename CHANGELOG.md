# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.3

- patch: Changed client

## 0.1.2

- patch: Added additional logging

## 0.1.1

- patch: Changed docker client load

## 0.1.0

- minor: Initial release
- patch: Disable tests
- patch: Disable tests
- patch: Initial commit

